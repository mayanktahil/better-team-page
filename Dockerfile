# Leverages https://hub.docker.com/r/mhart/alpine-node/ base image with yarn and dependencies. ~78 mb image
FROM mhart/alpine-node

# Listening port
EXPOSE 3000/tcp

# Directory to store git project 
RUN mkdir /app

# Copy code into container
COPY ./ /app

# Execute commands from said working directory
WORKDIR /app

# Install dependencies and build project artifacts
RUN npm install && npm run build

# Start service
CMD npm run start