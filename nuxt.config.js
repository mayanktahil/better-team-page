module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "better-team-page",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Better team page with availability information"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,500,600,700"
      }
    ]
  },
  plugins: ["~/plugins/vue-lazy-load"],
  modules: ["@nuxtjs/moment"],
  css: [{ src: "normalize.css", lang: "css" }],
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  router: {
    base: process.env.CI ? "/better-team-page/" : "/"
  },
  /*
   ** Build configuration
   */
  build: {
    transpile: ["vue-lazy-load", "lodash-es"],
    extractCSS: Boolean(process.env.CI),
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      config.resolve.alias["moment-timezone"] =
        "moment-timezone/builds/moment-timezone-with-data-10-year-range";

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  }
};
