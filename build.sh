#!/usr/bin/env bash
# Use the unofficial bash strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail; export FS=$'\n\t'

function download {
  curl --fail --location --remote-name --get "$@"
}

# Download cached team.json and new team.yml
download --data "job=build_page"  \
  'https://gitlab.com/gitlab-com/teampage-map/-/jobs/artifacts/master/raw/public/team.json' \
  || download https://gitlab-com.gitlab.io/teampage-map/team.json \
  || echo "No cached team.json available"
download https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml

# Update team.json with data from the team.yml
yarn
node ./add-timezone.js

# Build static page
rm -rf dist/
yarn run generate

# GZIP assets
find dist -type f | xargs gzip -f -k

mv dist public
