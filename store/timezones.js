import moment from "moment-timezone";

export const state = () => ({
  date: new Date(),
  hover: {}
});

const getTZOffset = (TZ, date) => {
  return moment.tz.zone(TZ).utcOffset(moment(date));
};

const formatOffset = offset => {
  const hours = Math.abs(Math.floor(offset / 60)) + "";
  const minutes = Math.abs(offset % 60) + "";
  return (
    (offset < 0 ? "+" : "-") +
    hours.padStart(2, "0") +
    ":" +
    minutes.padStart(2, "0")
  );
};

export const getters = {
  hoverState(state, getters) {
    const { posX, minutes, timestamp } = state.hover;

    const timestampNew = moment
      .tz(timestamp, getters.currentTimeZone)
      .add(minutes, "minutes");

    if (posX) {
      return {
        localTime: timestampNew.format("LT"),
        timestamp: timestampNew.format(),
        posX: posX,
        class: "hover"
      };
    }
    return {};
  },
  currentTimeZone() {
    return moment.tz.guess(true);
  },
  currentOffset(state, getters) {
    return getTZOffset(getters.currentTimeZone, state.date);
  },
  groupedByOffset(state, getters, rootState, rootGetters) {
    const grouped = rootGetters.sortedTeam.reduce(
      (all, curr) => {
        if (!rootGetters.visibility[curr.slug]) {
          return all;
        }
        const TZ = curr.TZ;

        const offset = getTZOffset(TZ, state.date);

        if (!all[offset]) {
          all[offset] = {
            offset,
            tanukis: [],
            TZ,
            order: offset
          };
        }

        all[offset].tanukis.push(curr);
        return all;
      },
      {
        [getters.currentOffset]: {
          offset: getters.currentOffset,
          tanukis: [],
          TZ: getters.currentTimeZone,
          order: -1000
        }
      }
    );

    return Object.values(grouped).sort((a, b) => a.order - b.order);
  },
  timeSlots: (state, getters) => {
    if (!getters.groupedByOffset || getters.groupedByOffset.length === 0) {
      return [];
    }

    const offsets = getters.groupedByOffset.map(x => x.offset);

    const date = moment(state.date).format("YYYY-MM-DD");

    const minOffset = Math.min(...offsets);
    const maxOffset = Math.max(...offsets);

    const startDate = getRoundedHour(
      moment(date + "T05:00:00" + formatOffset(minOffset))
    );

    const endDate = getRoundedHour(
      moment(date + "T23:00:00" + formatOffset(maxOffset))
    );

    const allDate = [];

    do {
      allDate.push(startDate.format());
      startDate.add(1, "h");
    } while (startDate.isSameOrBefore(endDate));

    return allDate;
  }
};

const getRoundedHour = date => {
  const newMoment = moment(date);
  const offset = newMoment.utcOffset();
  newMoment.add(Math.abs(offset % 60), "minutes");
  return newMoment;
};

export const mutations = {
  updateHover(state, message) {
    state.hover = message;
  }
};
