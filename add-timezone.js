const fs = require("fs");
const yaml = require("yaml-js");

const input = require("./team.json");
const geoTz = require("geo-tz");

geoTz.preCache(); // optionally load all features into memory

const doc = yaml.load(fs.readFileSync("./team.yml", "utf8"));

const matchMember = ({ name }) => {
  return doc.find(x => x.name === name);
};

input.team = input.team.map(teamMember => {
  if (!teamMember.TZ) {
    teamMember.TZ = geoTz(...teamMember.location)[0];
  }

  const matchedMember = matchMember(teamMember);

  if (matchedMember) {
    return {
      ...matchedMember,
      ...teamMember
    };
  }

  return teamMember;
});

input.version = `${input.version}-with-full-data`;

fs.writeFileSync("./static/team.json", JSON.stringify(input, null, 2));
